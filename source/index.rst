Welcome to Qt Programming Course's documentation!
=================================================
.. toctree::
  :maxdepth: 1
  :caption: Qt Course
  :name: sec-clickable

  tools/clickable

.. toctree::
  :maxdepth: 1
  :caption: Qt Course (legacy)
  :name: sec-about

  legacy/introduction/chapter-01


.. toctree::
   :maxdepth: 1
   :caption: Web App Development (legacy)
   :name: sec-webapp

   legacy/webapp/chapter-03-webapp

.. toctree::
  :maxdepth: 1
  :caption: Application Development  (legacy)
  :name: sec-qml

  legacy/qml/chapter-04-s01
  legacy/qml/chapter-04-s02
  legacy/qml/chapter-04-s03

.. toctree::
  :maxdepth: 1
  :caption: Advanced concepts  (legacy)
  :name: sec-advanced

  legacy/advanced/chapter-05-s01
  legacy/advanced/chapter-05-s02

.. toctree::
  :maxdepth: 1
  :caption: Tools  (legacy)
  :name: sec-tools

  legacy/about/compillation
  legacy/ubuntu-sdk/chapter-02

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
